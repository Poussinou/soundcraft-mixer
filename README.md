# Mixer Controller

Access digital mixer's control page via LAN.

> [SoundCraft](https://en.wikipedia.org/wiki/Soundcraft)'s [logo](https://www.logolynx.com/topic/soundcraft) is used as the app icon. All rights belong to their respective owners. No copyright infringement intended.
